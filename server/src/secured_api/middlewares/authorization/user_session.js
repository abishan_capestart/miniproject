const log = require('../../../../logger.js').LOG;
const util = require('../../../common/utilities');
var constants = require('../../../common/constants');


module.exports = function(req, res, next) {
    log.debug('Path => '+req.path);
    //let token = req.headers['x-access-token'];
    var token = util.getTokenFromRequest(req);

    if (!token) {
        log.debug('Request does not have a token!!!');
        res.status(constants.http_status.UNAUTHORIZED).json({"status": constants.http_status.UNAUTHORIZED});
    } 
    else if (util.isTokenExpire(req)) {
        log.debug('The token has expired!!!');
        // removeUserSession(req);
        res.status(constants.http_status.UNAUTHORIZED).json({"status": constants.http_status.UNAUTHORIZED});
    // } else if (!isSessionExisted(req)) {
    //     log.debug('The user session does NOT exist!!!');
    //     res.status(constants.http_status.UNAUTHORIZED).json({"status": constants.http_status.UNAUTHORIZED});
    } 
    else {
        log.debug('Everything looks good! Continue with the chain');
        const payload = util.getTokenPayload(req);
        req.user = payload.user;
        req.timezone_offset = payload.timezone_offset;
        next();
    }
};
