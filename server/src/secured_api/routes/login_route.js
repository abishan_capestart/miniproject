var ctrlLogin = require('../controllers/login_controller');

module.exports = function(router) {
    console.log('Login APIs');
    router
        .route('/api/login')
        .get(ctrlLogin.login);
    router
        .route('/api/login/:name/:age')
        .get(ctrlLogin.login);
    router
        .route('/api/login')
        .post(ctrlLogin.loginUsingPostMethod);
    router
        .route('/api/admin_login')
        .post(ctrlLogin.loginUsingPostMethod);
    router
        .route('/api/login')
        .put(ctrlLogin.loginUsingPutMethod);
    router
        .route('/api/login')
        .delete(ctrlLogin.loginUsingDeleteMethod);
    router
        .route('/api/team')
        .get(ctrlLogin.getTeam);
    router
        .route('/api/user_data')
        .get(ctrlLogin.getUserData);
    // router
    //     .route('/api/forgor_password')
    //     .post(ctrlLogin.forgotPasswrod);
    return router;
}


//GET -  1024 - passed through URL
//POST - form submitting - submitting via Object - payload
//PUT  - form submitting - submitting via Object
//DELETE - Submitting data for deletion
//OPTIONS