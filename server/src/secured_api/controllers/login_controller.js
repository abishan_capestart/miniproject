const loginRepo = require('../../repositories/login_repo');

module.exports.login = function(req, res) {

    const params = req.params;
    const name = params ? params.name : null;
    const ageAsText = params ? params.age : null;

    const query = req.query;
    const company = query ? query.company : null;

    let valid_data = true;

    res.setHeader('content-Type', 'text/plain');

    convertAge(ageAsText, function(age, valid_data){

        if(name && age){
            if(valid_data){
                res.statusCode = 200;
                res.write(`Hi ${name}. Your age is : ${age}`);
            } else {
                res.statusCode = 400;
                res.write(`Hi ${name}. You have passed invalid data`);
            }

            if(company){
                res.write(`Your company name is : ${company}`);
            }
        } else {
            res.write(`Welcome Guest`);
        }
        res.end();
    });
   
}

module.exports.loginUsingPostMethod = function(req, res) {

    // const params = req.params;
    // const name = params ? params.name : null;
    // const ageAsText = params ? params.age : null;

    // const query = req.query;
    // const company = query ? query.company : null;

    const payload = req.body;
    console.log(`req.body: ${JSON.stringify(req.body)}`);
    const name = payload ? payload.name : null;
    const ageAsText = payload ? payload.age : null;
    const company = payload ? payload.company : null;

    console.log(`name: ${name}`);
    console.log(`age: ${ageAsText}`);
    console.log(`company: ${company}`);

    let valid_data = true;

    res.setHeader('content-Type', 'text/plain');

    convertAge(ageAsText, function(age, valid_data){

        if(name && age){
            if(valid_data){
                res.statusCode = 200;
                //res.write(`Hi ${name}. Your age is : ${age}`);
                loginRepo.getUserData(function(error, result){
                    console.log(`User data fetched successfully`);
                    if(error){
                        res.write(`Error Occurred: ${{error}}`);
                        loginRepo.updateUserData(function(error, result){
                            console.log(`User data updated successfully`);
                            loginRepo.deleteUserData(function(error, result){
                                console.log(`User data deleted successfully`);
                            });
                        });
                    }
                    if(result){        
                        res.write(`Result: ${JSON.stringify(result)}}`);
                    }
                    res.end();
                })


            } else {
                res.statusCode = 400;
                res.write(`Hi ${name}. You have passed invalid data`);
            }

            if(company){
                res.write(`Your company name is : ${company}`);
            }
        } else {
            res.write(`Welcome Guest`);
            res.end();
        }
    });
   
}

module.exports.loginUsingPutMethod = function(req, res) {

    const payload = req.body;
    console.log(`req.body: ${JSON.stringify(req.body)}`);
    const name = payload ? payload.name : null;
    const ageAsText = payload ? payload.age : null;
    const company = payload ? payload.company : null;

    console.log(`PUT method is called`);

    console.log(`name: ${name}`);
    console.log(`age: ${ageAsText}`);
    console.log(`company: ${company}`);

    let valid_data = true;

    res.setHeader('content-Type', 'text/plain');
    res.write(`PUT method is called`);  
    res.end();
}

module.exports.loginUsingDeleteMethod = function(req, res) {

    const payload = req.body;
    console.log(`req.body: ${JSON.stringify(req.body)}`);
    const name = payload ? payload.name : null;
    const ageAsText = payload ? payload.age : null;
    const company = payload ? payload.company : null;

    console.log(`DELETE method is called`);

    console.log(`name: ${name}`);
    console.log(`age: ${ageAsText}`);
    console.log(`company: ${company}`);

    let valid_data = true;

    res.setHeader('content-Type', 'text/plain');
    res.write(`DELETE method is called`);  
    res.end();
}

module.exports.getUserData = function(req, res) {

    res.setHeader('content-Type', 'text/plain');

    loginRepo.getUserData(function(error, result){
        if(error){
            res.statusCode = 200;
            console.log(`Error Occurred: ${JSON.stringify(error)}`);
            res.write(`Error Occurred: ${{error}}`);
            res.end();
        }
        if(result){
            const records = result.rows ? result.rows : [];
            res.statusCode = 200;
            res.write(`Result: ${JSON.stringify(records)}}`);
            res.end();
        }
    });
   
}

function convertAge(ageAsText, callback){
    
    let valid_data = true;
    console.log(`isNaN(age): ${isNaN(ageAsText)}`);
    if(isNaN(ageAsText)){
        valid_data = false;
    } else {
         if(ageAsText<1 || ageAsText>120){
            valid_data = false;
         }
    }
    callback(ageAsText, valid_data);
}

module.exports.getTeam = function(req, res) {
    const respObj = {
        status: 'SUCCESS',
        message: "Team name fetched successfully",
        data: "Phase 01 Training"
      };
      res.status(200).json(respObj);
}