var ctrlEvents = require('../controllers/eventcontrollers');

module.exports = function(router) {
    router
        .route('/employees')
        .get(ctrlEvents.getEventsUser);
    router
        .route('/employees/:empid')
        .get(ctrlEvents.getParticularUser);
    router
        .route('/deleteemployee/:empid')
        .get(ctrlEvents.deleteSpecificUser);
        router
        .route('/updateemployee/:empid/:salary')
        .get(ctrlEvents.updateSpecificUser);
}