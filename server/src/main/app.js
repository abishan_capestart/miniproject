const express = require('express');
const indexApiRouter=require('../open_api/routes')

const bodyparser = require('body-parser');
const mysql = require('mysql');
var app = express();
app.use(bodyparser.json());

app.use('/',indexApiRouter);
app.listen(3000, ()=> console.log('Express server is running successfully'));